package br.unifacisa.p3.lista.encadeada;

import java.io.InvalidObjectException;

import br.unifacisa.p3.core.BaseEncadeada;
import br.unifacisa.p3.core.No;

public class Lista<T> extends BaseEncadeada<T> {

	/**
	 * Adiciona um objeto na fila
	 * @param obj
	 * @throws InvalidObjectException
	 */
	public void add(T obj) throws InvalidObjectException {
		if (obj == null) {
			throw new InvalidObjectException("Objeto nulo");
		} else if (inicio == null) {
			inicio = new No<T>(obj);
			ultimo = inicio;
			inseridos++;
		} else {
			No<T> noObj = new No<T>(obj);
			noObj.setAnterior(ultimo);
			ultimo.setProximo(noObj);
			ultimo = ultimo.getProximo();
			inseridos++;
		}
	}
	
	/**
	 * Adiciona um objeto na fila pelo index
	 * @param index
	 * @param obj
	 * @throws InvalidObjectException
	 */
	public void add(int index, T obj) throws InvalidObjectException {
		if (obj == null) {
			throw new InvalidObjectException("Objeto nulo");
		} else if (inicio == null) {
			add(obj);
		} else if (index < 0 || index > this.inseridos) {
			throw new ArrayIndexOutOfBoundsException();
		} else if (index == 0) {
			addInicio(obj);
		} else if (index == inseridos) {
			add(obj);
		} else {
			No<T> aux = inicio;
			for (int i = 0; i < index - 1; i++) {
				aux = aux.getProximo();
			}
			No<T> objNo = new No<T>(obj, aux.getProximo());
			aux.getProximo().setAnterior(objNo);
			objNo.setAnterior(aux);
			aux.setProximo(objNo);
			inseridos++;
		}

	}
	
	/**
	 * Concatena 2 listas
	 * @param lista
	 */
	public void concat(Lista<T> lista) {
		this.ultimo.setProximo(lista.getInicio());
		inseridos += lista.size();
	}
	
	/**
	 * Retorna um elemento da lista
	 * @param index
	 * @return obj
	 */
	public T get(int index) {
		T element = null;
		if (index < 0 || index > this.inseridos) {
			throw new ArrayIndexOutOfBoundsException();
		} else if (index == 0) {
			element = this.inicio.getObj();
		} else {
			No<T> aux = inicio;
			for (int i = 0; i < index - 1; i++) {
				aux = aux.getProximo();
			}
			element = aux.getProximo().getObj();
		}
		return element;
	}
	
	/**
	 *  Remove um objeto da fila pelo index
	 * @param index
	 */
	public void remove(int index) {
		if (index < 0 || index > this.inseridos) {
			throw new ArrayIndexOutOfBoundsException();
		} else if (index == 0) {
			this.inicio = this.inicio.getProximo();
			inseridos--;
		} else {
			No<T> aux = inicio;
			for (int i = 0; i < index - 1; i++) {
				aux = aux.getProximo();
			}
			if (aux.getProximo() == null) {
				aux = null;
			} else {
				aux.setProximo(aux.getProximo().getProximo());
			}
			inseridos--;
		}
	}

	/**
	 * Remove um objeto da fila de acordo com o objeto
	 * @param obj
	 * @throws InvalidObjectException
	 */
	
	public void remove(T obj) throws InvalidObjectException {
		if (obj == null) {
			throw new InvalidObjectException("Objeto nulo");
		}else if (inicio.getObj().equals(obj)) {
			inicio = null;
			inseridos--;
		}
		else {
			No<T> aux = inicio;
			while (aux.getProximo() != null && !obj.equals(aux.getProximo().getObj())) {
				aux = aux.getProximo();
			}
			if (aux.getProximo() == null) {
				aux = null;
			} else {
				if (obj.equals(aux.getProximo().getObj())) aux.setProximo(aux.getProximo().getProximo());
			}
			inseridos--;
		
		}
		
	}
	
	/**
	 * Retorna o index de um objeto
	 * @param obj
	 * @return int
	 */
	public int indexOf(T obj) {
		No<T> aux = this.inicio;
		return indexOfRecursivo(aux, obj, 0);

	}
	
	private int indexOfRecursivo(No<T> aux, T obj, int index) {
		if (aux.getObj().equals(obj)) {
			return index;
		} else if (aux.getProximo() == null) {
			return -1;
		}
		index = this.indexOfRecursivo(aux.getProximo(), obj, ++index);
		return index;
	}
	
	/**
	 * Adiciona um objeto no inicio da fila
	 * @param obj
	 */
	public void addInicio(T obj) {
		inicio = new No<T>(obj, this.inicio);
		inseridos++;
	}
	
	/**
	 * Retorna um elemento da fila
	 * @param element
	 * @return	obj
	 */
	public T get(T element) {
		No<T> aux = this.inicio;
		while (aux.getProximo() != null) {
			aux = aux.getProximo();
		}
		return aux.getObj();
	}

}
