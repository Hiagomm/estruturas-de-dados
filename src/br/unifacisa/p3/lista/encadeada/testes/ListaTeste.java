package br.unifacisa.p3.lista.encadeada.testes;

import static org.junit.jupiter.api.Assertions.*;

import java.io.InvalidObjectException;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import br.unifacisa.p3.lista.encadeada.Lista;

class ListaTeste {
	private Lista<String> lista;

	@BeforeEach
	void setUp() throws Exception {
		this.lista = new Lista<String>();
	}

	@AfterEach
	void tearDown() throws Exception {
		this.lista = new Lista<String>();
	}

	@Test
	void addTest() throws InvalidObjectException {
		this.lista.add("1");
		this.lista.add("1");
		this.lista.add("1");
		assertEquals(3, lista.size());
	}

	@Test
	void addTest2() throws InvalidObjectException {
		this.lista.add("1");
		this.lista.add("2");
		this.lista.add("3");
		assertEquals("2", lista.get(1));
	}
	
	@Test
	void addTest3() throws InvalidObjectException {
		this.lista.add("1");
		this.lista.add("2");
		this.lista.add("3");
		this.lista.remove(1);
		assertEquals("3", lista.get(1));
	}
	
	@Test
	void addTest4() throws InvalidObjectException {
		this.lista.add("1");
		this.lista.add("1");
		this.lista.add(1,"2");
		this.lista.add("3");
		assertEquals("2", lista.get(1));
	}
	
	@Test
	void addTest5() throws InvalidObjectException {
		this.lista.add("1");
		assertThrows(ArrayIndexOutOfBoundsException.class, () -> {
			this.lista.add(-1,"1");
		});
	}
	
	@Test
	void addTest6() throws InvalidObjectException {
		this.lista.add("1");
		assertThrows(ArrayIndexOutOfBoundsException.class, () -> {
			this.lista.add(2,"1");
		});
	}
	
	@Test
	void addTest7() throws InvalidObjectException {
		assertThrows(InvalidObjectException.class, () -> {
			this.lista.add(null);
		});
	}
	@Test
	void addTest8() throws InvalidObjectException {
		assertThrows(InvalidObjectException.class, () -> {
			this.lista.add(1,null);
		});
	}
	
	@Test
	void emptyTest() {
		assertTrue(lista.isEmpty());
	}
	
	@Test
	void emptyTest2() throws InvalidObjectException {
		lista.add("1");
		assertFalse(lista.isEmpty());
	}
	
	@Test
	void sizeTest() {
		assertEquals(0, lista.size());
	}
	
	@Test
	void sizeTest2() throws InvalidObjectException {
		lista.add("1");
		assertEquals(1, lista.size());
	}
	@Test
	void sizeTest3() throws InvalidObjectException {
		lista.add("1");
		lista.remove(0);
		assertEquals(0, lista.size());
	}
	@Test
	void getTest() throws InvalidObjectException {
		lista.add("1");
		assertEquals("1",lista.get(0));
	}
	@Test
	void getTest2() throws InvalidObjectException {
		this.lista.add("1");
		assertThrows(ArrayIndexOutOfBoundsException.class, () -> {
			this.lista.get(-1);
		});
	}
	
	@Test
	void getTest3() throws InvalidObjectException {
		this.lista.add("1");
		assertThrows(ArrayIndexOutOfBoundsException.class, () -> {
			this.lista.get(2);
		});
	}
	@Test
	void getTest4() throws InvalidObjectException {
		lista.add("1");
		lista.add("2");
		lista.add("3");
		assertEquals("2",lista.get(1));
	}	
	
	@Test
	void indexOfTest() throws InvalidObjectException {
		lista.add("1");
		lista.add("2");
		lista.add("3");
		assertEquals(1, lista.indexOf("2"));
	}
	@Test
	void indexOfTest2() throws InvalidObjectException {
		lista.add("1");
		lista.add("2");
		lista.add("3");
		assertEquals(-1, lista.indexOf("a"));
	}
	
	@Test
	void removeTest() throws InvalidObjectException {
		lista.add("1");
		lista.add("2");
		lista.add("3");
		lista.remove("2");
		assertEquals("3", lista.get(1));
	}
	
	@Test
	void removeTest2() throws InvalidObjectException {
		lista.add("1");
		lista.add("2");
		lista.add("3");
		lista.remove("0");
		
	}
	@Test
	void removeTest3() throws InvalidObjectException {
		lista.add("1");
		lista.add("2");
		lista.add("3");
		lista.remove(0);
		assertEquals("3", lista.get(1));
	}
	@Test
	void removeTest4() throws InvalidObjectException {
		lista.add("1");
		lista.add("2");
		lista.add("3");
		lista.remove(2);
		assertEquals("2", lista.get(1));
	}
	@Test
	void removeTest5() throws InvalidObjectException {
		lista.add("1");
		lista.add("2");
		lista.add("3");
		lista.remove(1);
		assertEquals("3", lista.get(1));
	}
	
	@Test
	void concat() throws InvalidObjectException {
		Lista<String> aux = new Lista<String>();
		aux.add("123");
		aux.add("123");
		lista.add("123");
		lista.concat(aux);
		assertEquals(3, lista.size());
	}
}
