package br.unifacisa.p3.fila.encadeada.testes;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.InvalidObjectException;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import br.unifacisa.p3.fila.encadeada.Fila;

class FilaTest {
	
	private Fila<String> fila;
	
	@BeforeEach
	void setUp() throws Exception {
		this.fila = new Fila<String>();
	}

	@AfterEach
	void tearDown() throws Exception {
		this.fila = new Fila<String>();
	}

	@Test
	void enQueueTest() throws InvalidObjectException {
		fila.enQueue("a");
		assertEquals("a",fila.front());
	}
	@Test
	void enQueueTest2() throws InvalidObjectException {
		fila.enQueue("a");
		fila.enQueue("b");
		fila.enQueue("c");
		assertEquals("a",fila.front());
	}

	@Test
	void enQueueTest3() throws InvalidObjectException {
		fila.enQueue("a");
		fila.enQueue("b");
		fila.enQueue("c");
		fila.deQueue();
		assertEquals("b",fila.front());
	}
	@Test
	void deQueueTest4() throws InvalidObjectException {
		fila.enQueue("a");
		fila.enQueue("b");
		fila.enQueue("c");
		fila.deQueue();
		assertEquals(2,fila.size());
	}
	@Test
	void deQueueTest5() throws InvalidObjectException {
		fila.enQueue("a");
		fila.enQueue("b");
		fila.enQueue("c");
		fila.deQueue();
		fila.deQueue();
		assertEquals(1,fila.size());
	}
	@Test
	void deQueueTest6() throws InvalidObjectException {
		fila.enQueue("a");
		fila.enQueue("b");
		fila.enQueue("c");
		assertEquals("a",fila.deQueue());
	}
	@Test
	void deQueueTest7() throws InvalidObjectException {
		fila.enQueue("a");
		fila.enQueue("b");
		fila.enQueue("c");
		fila.deQueue();
		assertEquals("b",fila.deQueue());
	}
	@Test
	void deQueueTest8() throws InvalidObjectException {
		fila.enQueue("a");
		fila.enQueue("b");
		fila.enQueue("c");
		assertEquals("a",fila.deQueue());
	}
	
	@Test
	void frontTest9() throws InvalidObjectException {
		fila.enQueue("a");
		fila.enQueue("b");
		fila.enQueue("c");
		assertEquals("a",fila.front());
	}
	@Test
	void frontTest2() throws InvalidObjectException {
		fila.enQueue("a");
		fila.enQueue("b");
		fila.enQueue("c");
		fila.deQueue();
		assertEquals("b",fila.front());
	}
	
	@Test
	void frontTest3() throws InvalidObjectException {
		fila.enQueue("a");
		fila.enQueue("b");
		fila.enQueue("c");
		fila.deQueue();
		fila.deQueue();
		assertEquals("c",fila.front());
	}

	@Test
	void emptyTest() {
		assertTrue(fila.isEmpty());
	}
	
	@Test
	void emptyTest2() throws InvalidObjectException {
		fila.enQueue("1");
		assertFalse(fila.isEmpty());
	}
	
	@Test
	void emptyTest3() throws InvalidObjectException {
		fila.enQueue("1");
		fila.deQueue();
		assertTrue(fila.isEmpty());
	}
	
	@Test
	void emptyTest4() throws InvalidObjectException {
		fila.enQueue("1");
		fila.front();
		assertFalse(fila.isEmpty());
	}
	
	@Test
	void sizeTest() {
		assertEquals(0, fila.size());
	}
	
	@Test
	void sizeTest2() throws InvalidObjectException {
		fila.enQueue("1");
		assertEquals(1, fila.size());
	}
	@Test
	void sizeTest3() throws InvalidObjectException {
		fila.enQueue("1");
		fila.deQueue();
		assertEquals(0, fila.size());
	}
	@Test
	void sizeTest4() throws InvalidObjectException {
		fila.enQueue("1");
		fila.front();
		assertEquals(1, fila.size());
	}
	@Test
	void sizeTest5() throws InvalidObjectException {
		fila.enQueue("1");
		fila.enQueue("1");
		fila.enQueue("1");
		fila.enQueue("1");
		fila.enQueue("1");
		fila.enQueue("1");
		assertEquals(6, fila.size());
	}

}
