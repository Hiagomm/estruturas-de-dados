package br.unifacisa.p3.fila.encadeada;

import java.io.InvalidObjectException;

import br.unifacisa.p3.core.BaseEncadeada;
import br.unifacisa.p3.core.No;

public class Fila<T> extends BaseEncadeada<T> {

	/**
	 * Insere um obj na fila
	 * 
	 * @param obj
	 * @throws InvalidObjectException
	 */
	public void enQueue(T obj) throws InvalidObjectException {
		if (obj == null) {
			throw new InvalidObjectException("Objeto nulo");
		} else if (this.inicio == null) {
			this.inicio = new No<T>(obj);
			this.ultimo = inicio;
			inseridos++;
		} else {
			ultimo.setProximo(new No<T>(obj));
			ultimo = ultimo.getProximo();
			inseridos++;
		}
	}

	/**
	 * Checa o primeiro elemento da fila
	 * 
	 * @return obj
	 */
	public T front() {
		return inicio.getObj();
	}

	/**
	 * Remove o ultimo elemento da fila
	 * 
	 * @return obj
	 */
	public T deQueue() {
		T obj = inicio.getObj();
		inicio = inicio.getProximo();
		inseridos--;
		return obj;
	}

}
