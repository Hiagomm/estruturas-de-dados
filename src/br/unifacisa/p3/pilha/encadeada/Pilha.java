package br.unifacisa.p3.pilha.encadeada;

import java.io.InvalidObjectException;
import java.util.EmptyStackException;

import br.unifacisa.p3.core.BaseEncadeada;
import br.unifacisa.p3.core.No;

public class Pilha<T> extends BaseEncadeada<T> {

	/**
	 * Insere um elemento na pilha
	 * @param obj
	 * @throws InvalidObjectException
	 */
	public void push(T obj) throws InvalidObjectException {
		if (obj == null) {
			throw new InvalidObjectException("Objeto nulo");
		} else if (this.inicio == null) {
			this.inicio = new No<T>(obj);
			inseridos++;
		} else {
			this.inicio = new No<T>(obj, inicio);
			inseridos++;
		}

	}

	/**
	 * Remove e retorna o primeiro elemento da pilha
	 * @return obj
	 */
	public T pop() {
		if (this.inicio == null) {
			throw new EmptyStackException();
		} else {
			T obj = inicio.getObj();
			inicio = inicio.getProximo();
			inseridos--;
			return obj;
		}
	}
	/** Retorna o primeiro elemento da pilha
	 * return obj
	 */
	public T top() {
		return this.inicio.getObj();
	}
	/**
	 * Limpa a pilha
	 */
	public void clear() {
		this.inicio = null;
		inseridos = 0;
	}
	
}
