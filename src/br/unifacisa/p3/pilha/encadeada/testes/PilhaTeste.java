package br.unifacisa.p3.pilha.encadeada.testes;

import static org.junit.jupiter.api.Assertions.*;

import java.io.InvalidObjectException;
import java.util.EmptyStackException;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import br.unifacisa.p3.pilha.encadeada.Pilha;

class PilhaTeste {

	private Pilha<String> pilha;

	@BeforeEach
	void setUp() throws Exception {
		pilha = new Pilha<String>();
	}

	@AfterEach
	void tearDown() throws Exception {
		pilha = new Pilha<String>();
	}

	@Test
	void pushTest() throws InvalidObjectException {
		pilha.push("1");
		pilha.push("2");
		pilha.push("3");
		assertEquals("3", pilha.top());
	}

	@Test
	void pushTest2() throws InvalidObjectException {
		pilha.push("1");
		pilha.push("2");
		pilha.push("3");
		assertThrows(InvalidObjectException.class, () -> {
			pilha.push(null);
		});
	}

	@Test
	void pushTeste3() throws InvalidObjectException {
		pilha.push("1");
		pilha.push("2");
		pilha.push("3");
		assertEquals(3, pilha.size());
	}
	
	@Test
	void popTest() {
		assertThrows(EmptyStackException.class,() -> {
			pilha.pop();
		});
	}
	@Test
	void popTest2() throws InvalidObjectException {
		pilha.push("1");
		assertEquals("1", pilha.pop());
	}
	@Test
	void popTest3() throws InvalidObjectException {
		pilha.push("1");
		pilha.pop();
		assertEquals(0,pilha.size());
	}
	
	@Test
	void popTest4() throws InvalidObjectException {
		pilha.push("1");
		pilha.push("1");
		pilha.push("1");
		pilha.pop();
		assertEquals(2,pilha.size() );
	}
	void popTest5() throws InvalidObjectException {
		pilha.push("1");
		pilha.push("2");
		assertEquals("2", pilha.pop());
	}
	
	@Test
	void popTest6() throws InvalidObjectException {
		pilha.push("1");
		pilha.top();
		pilha.pop();
		assertEquals(0,pilha.size());
	}
	@Test 
	void topTest() throws InvalidObjectException {
		pilha.push("1");
		pilha.push("2");
		assertEquals("2", pilha.top());
	}
	void topTest2() throws InvalidObjectException {
		pilha.push("1");
		pilha.push("2");
		pilha.top();
		assertEquals(2, pilha.size());
	}
	
	void topTest3() throws InvalidObjectException {
		pilha.push("1");
		pilha.push("2");
		pilha.pop();
		assertEquals("1", pilha.top());
	}
	
	@Test
	void clearTest() throws InvalidObjectException {
		pilha.push("1");
		pilha.push("2");
		pilha.push("1");
		pilha.push("2");
		pilha.clear();
		assertEquals(0, pilha.size());
	}
	@Test
	void emptyTest() {
		assertTrue(pilha.isEmpty());
	}

	@Test
	void emptyTest2() throws InvalidObjectException {
		pilha.push("1");
		assertFalse(pilha.isEmpty());
	}
	
	@Test
	void emptyTest3() throws InvalidObjectException {
		pilha.push("1");
		pilha.top();
		assertFalse(pilha.isEmpty());
	}
	
	@Test
	void emptyTest4() throws InvalidObjectException {
		pilha.push("1");
		pilha.pop();
		assertTrue(pilha.isEmpty());
	}

	@Test
	void sizeTest() {
		assertEquals(0, pilha.size());
	}

	@Test
	void sizeTest2() throws InvalidObjectException {
		pilha.push("1");
		assertEquals(1, pilha.size());
	}

	@Test
	void sizeTest3() throws InvalidObjectException {
		pilha.push("1");
		pilha.pop();
		assertEquals(0, pilha.size());
	}
	
	@Test
	void sizeTest4() throws InvalidObjectException {
		pilha.push("1");
		pilha.top();
		assertEquals(1, pilha.size());
	}
	
	@Test
	void sizeTest5() throws InvalidObjectException {
		pilha.push("1");
		pilha.push("1");
		pilha.push("1");
		pilha.clear();
		assertEquals(0, pilha.size());
	}

	@Test
	void sizeTest6() throws InvalidObjectException {
		pilha.push("1");
		pilha.push("1");
		pilha.push("1");
		pilha.pop();
		assertEquals(2, pilha.size());
	}

}
