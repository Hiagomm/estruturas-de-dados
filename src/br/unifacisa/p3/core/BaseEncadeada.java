package br.unifacisa.p3.core;

public abstract class BaseEncadeada<T> {

	protected No<T> inicio;
	protected No<T> ultimo;
	protected int inseridos;

	/**
	 * Quantidade de nós
	 * @return int
	 */
	public int size() {
		return inseridos;
	}

	/**
	 * Se esta vazio
	 * @return boolean
	 */
	public boolean isEmpty() {
		return this.inicio == null;
	}

	public String toString() {
		StringBuilder list = new StringBuilder();
		list.append("[");
		No<T> aux = inicio;
		if (aux != null) {

			while (aux.getProximo() != null) {
				list.append(aux);
				list.append(", ");
				aux = aux.getProximo();
			}
			list.append(aux);
		}
		list.append("]");
		return list.toString();
	}

	/**
	 * Nó inicial
	 * @return No
	 */
	public No<T> getInicio() {
		return inicio;
	}
}
