package br.unifacisa.p3.core;

public class No<T> {

	private T obj;
	private No<T> proximo;
	private No<T> anterior;

	public No(T obj) {
		this.obj = obj;
	}

	public No() {
	}

	public No(T obj, No<T> proximo) {
		super();
		this.obj = obj;
		this.proximo = proximo;
	}
	
	@Override
	public String toString() {
		T anterior = this.anterior != null ? this.anterior.getObj():null;
		T proximo = this.proximo != null ? this.proximo.getObj() : null ;
		return "[" + "anterior=" + anterior+  
				" obj=" + obj + 
				", proximo="+proximo+ "]";
	}

	/**
	 * @return o obj
	 */
	public T getObj() {
		return obj;
	}

	/**
	 * @param seta o obj
	 */
	public void setObj(T obj) {
		this.obj = obj;
	}

	/**
	 * @return o proximo
	 */
	public No<T> getProximo() {
		return proximo;
	}

	/**
	 * @param seta o proximo
	 */
	public void setProximo(No<T> proximo) {
		this.proximo = proximo;
	}

	/**
	 * @return o anterior
	 */
	public No<T> getAnterior() {
		return anterior;
	}

	/**
	 * @param seta o anterior
	 */
	public void setAnterior(No<T> anterior) {
		this.anterior = anterior;
	}

}
