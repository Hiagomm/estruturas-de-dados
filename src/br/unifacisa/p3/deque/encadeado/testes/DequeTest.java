package br.unifacisa.p3.deque.encadeado.testes;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.InvalidObjectException;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import br.unifacisa.p3.deque.encadeado.Deque;
import br.unifacisa.p3.deque.encadeado.EmptyDequeException;

class DequeTest {

	private Deque<String> deque;

	@BeforeEach
	void setUp() throws Exception {
		deque = new Deque<String>();
	}

	@AfterEach
	void tearDown() throws Exception {
		deque = new Deque<String>();
	}

	@Test
	void insertTest() throws InvalidObjectException {
		this.deque.insertFirst("1");
		this.deque.insertFirst("1");
		this.deque.insertFirst("1");
		assertEquals(3, deque.size());
	}

	@Test
	void insertTest2() throws EmptyDequeException, InvalidObjectException {
		this.deque.insertFirst("1");
		this.deque.insertFirst("2");
		this.deque.insertFirst("3");
		assertEquals("3", deque.removeFirst());
	}

	@Test
	void insertTest3() throws InvalidObjectException, EmptyDequeException {
		this.deque.insertFirst("1");
		this.deque.insertFirst("2");
		this.deque.insertLast("3");
		assertEquals("2", deque.removeFirst());
	}

	@Test
	void insertTest4() throws InvalidObjectException, EmptyDequeException {
		this.deque.insertFirst("1");
		this.deque.insertFirst("2");
		this.deque.insertLast("3");
		assertEquals("2", deque.removeFirst());
	}

	@Test
	void insertTest5() throws InvalidObjectException, EmptyDequeException {
		this.deque.insertLast("3");
		this.deque.insertFirst("1");
		this.deque.insertFirst("2");
		assertEquals("2", deque.removeFirst());
	}

	@Test
	void removeTest() throws InvalidObjectException, EmptyDequeException {
		this.deque.insertFirst("2");
		assertEquals("2", deque.removeFirst());
	}

	@Test
	void removeTest2() throws InvalidObjectException, EmptyDequeException {
		this.deque.insertFirst("2");
		this.deque.insertLast("1");
		assertEquals("2", deque.removeFirst());
	}

	@Test
	void removeTest3() throws InvalidObjectException, EmptyDequeException {
		this.deque.insertFirst("2");
		this.deque.insertLast("1");
		assertEquals("1", deque.removeLast());
	}

	@Test
	void removeTest4() throws InvalidObjectException, EmptyDequeException {
		this.deque.insertFirst("2");
		this.deque.insertLast("1");
		this.deque.insertLast("1");
		deque.removeByValue("1");
		assertEquals("1", deque.removeLast());
	}

	@Test
	void removeTest5() throws InvalidObjectException, EmptyDequeException {
		this.deque.insertFirst("2");
		this.deque.insertLast("1");
		this.deque.insertLast("1");
		deque.removeByValue("1");
		assertEquals("2", deque.removeFirst());
	}

	@Test
	void removeTest6() throws InvalidObjectException, EmptyDequeException {
		this.deque.insertFirst("2");
		this.deque.insertLast("1");
		this.deque.insertLast("1");
		deque.removeByValue("1");
		assertEquals("1", deque.removeLast());
	}
	@Test
	void removeTest7() throws InvalidObjectException, EmptyDequeException {
		this.deque.insertFirst("2");
		this.deque.insertLast("1");
		this.deque.insertLast("1");
		deque.removeByIndex(1);
		assertEquals("1", deque.removeLast());
	}
	
	@Test
	void removeTest8() throws InvalidObjectException, EmptyDequeException {
		this.deque.insertFirst("2");
		this.deque.insertLast("1");
		deque.removeByIndex(0);
		assertEquals("1", deque.removeFirst());
	}

	@Test
	void emptyTest() {
		assertTrue(deque.isEmpty());
	}
	
	@Test
	void emptyTest2() throws InvalidObjectException {
		deque.insertFirst("1");
		assertFalse(deque.isEmpty());
	}
	
	@Test
	void sizeTest() {
		assertEquals(0, deque.size());
	}
	
	@Test
	void sizeTest2() throws InvalidObjectException {
		deque.insertLast("1");
		assertEquals(1, deque.size());
	}
	@Test
	void sizeTest3() throws InvalidObjectException, EmptyDequeException {
		deque.insertLast("1");
		deque.removeLast();
		assertEquals(0, deque.size());
	}

	@Test
	void sizeTest4() throws InvalidObjectException, EmptyDequeException {
		deque.insertFirst("1");
		deque.removeFirst();
		assertEquals(0, deque.size());
	}
	
	@Test
	void sizeTest5() throws InvalidObjectException, EmptyDequeException {
		deque.insertFirst("1");
		deque.removeFirst();
		deque.insertLast("1");
		deque.removeLast();
		assertEquals(0, deque.size());
	}
}
