package br.unifacisa.p3.deque.encadeado;

import java.io.InvalidObjectException;

import br.unifacisa.p3.core.BaseEncadeada;
import br.unifacisa.p3.core.No;

public class Deque<T> extends BaseEncadeada<T> {

	/**
	 * Remove o primeiro elemento do deque
	 * @return obj
	 * @throws EmptyDequeException
	 */
	public T removeFirst() throws EmptyDequeException {
		if (inicio == null) {
			throw new EmptyDequeException("Deque vazio");
		} else {
			T element = inicio.getObj();
			inicio = inicio.getProximo();
			inseridos--;
			return element;
		}
	}
	
	/**
	 * Remove o ultimo elemento do deque
	 * @return obj
	 * @throws EmptyDequeException
	 */

	public T removeLast() throws EmptyDequeException {
		if (inicio == null) {
			throw new EmptyDequeException("Deque vazio");
		} else {
			T element = ultimo.getObj();
			No<T> aux = inicio;
		
			while (aux.getProximo() != null) {
				aux = aux.getProximo();
			}
			if (aux != null) aux.setProximo(null);
			ultimo = aux;
			
			inseridos--;
			return element;
		}
	}
	
	/**
	 * Insere o obj na ultima posicao
	 * @param obj
	 * @throws InvalidObjectException
	 */
	public void insertLast(T obj) throws InvalidObjectException {
		if (obj == null) {
			throw new InvalidObjectException("Objeto nulo");
		} else if (inicio == null) {
			inicio = new No<T>(obj);
			ultimo = inicio;
			inseridos++;
		} else {
			ultimo.setProximo(new No<T>(obj));
			ultimo = ultimo.getProximo();
			inseridos++;
		}
	}
	
	/**
	 * Insere o obj na primeira posicao
	 * @param obj
	 * @throws InvalidObjectException
	 */

	public void insertFirst(T obj) throws InvalidObjectException {
		if (obj == null) {
			throw new InvalidObjectException("Objeto nulo");
		} else {
			inicio = new No<T>(obj, this.inicio);
			ultimo = inicio;
			inseridos++;
		}
	}
	
	/**
	 * Remove pelo obj
	 * @param obj
	 * @throws InvalidObjectException
	 */
	public void removeByValue(T obj) throws InvalidObjectException {
		if (obj == null) {
			throw new InvalidObjectException("Objeto nulo");
		} else if (inicio.getObj().equals(obj)) {
			inicio = null;
			inseridos--;
		} else {
			No<T> aux = inicio;
			while (aux.getProximo() != null && !obj.equals(aux.getProximo().getObj())) {
				aux = aux.getProximo();
			}
			if (aux.getProximo() == null) {
				aux = null;
			} else {
				if (obj.equals(aux.getProximo().getObj())) {
					aux.setProximo(aux.getProximo().getProximo());
					inseridos--;
				}
			}

		}
	}

	/**
	 * Remove pelo index
	 * @param index
	 */
	public void removeByIndex(int index) {
		if (index < 0 || index > this.inseridos) {
			throw new ArrayIndexOutOfBoundsException();
		} else if (index == 0) {
			this.inicio = this.inicio.getProximo();
			inseridos--;

		} else {
			No<T> aux = inicio;
			for (int i = 0; i < index - 1; i++) {
				aux = aux.getProximo();
			}
			if (aux.getProximo() == null) {
				aux = null;
			} else {
				aux.setProximo(aux.getProximo().getProximo());
			}
			inseridos--;
		}
	}

}