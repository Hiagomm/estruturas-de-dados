package br.unifacisa.p3.deque.encadeado;

public class EmptyDequeException extends Exception {

	public EmptyDequeException(String string) {
		super(string);
	}

	private static final long serialVersionUID = 1L;

	
}
